package Controllers;

import Misc.ControlledView;
import Misc.DBCaller;
import Misc.Main;
import Misc.Util;
import Model.PatientHistory;
import Model.PatientProfile;
import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DoctorController  implements ControlledView {
	List<PatientProfile> patientList = new ArrayList<PatientProfile>();
	ObservableList<PatientHistory> patientHistory = FXCollections.observableArrayList();
	ObservableList<Integer> IDList = FXCollections.observableArrayList();	
	 @FXML	 TextField txtpatientName = new TextField(); 
	 @FXML	TextField txtpatientAge = new TextField();
	 @FXML	 TextField txtpatientAddress = new TextField(); 
	 @FXML	TextField txtpatientAdditionalDrugs = new TextField();
	 @FXML	 ComboBox<Integer> cmbPatientID = new ComboBox<Integer>();
	@FXML	 ComboBox<String> ModeBox = new ComboBox<String>();
	@FXML	TextField txtpatientContactNo = new TextField();
	 @FXML	 Button btnLoad = new Button();
	 @FXML	 Button btnSave = new Button();
	 @FXML	 Button btnGraphView = new Button();
	 @FXML	 Button btnEnableAddtion = new Button();
	 @FXML	 Button btnSaveNewPatient = new Button();
	 @FXML	 Button btnGetHistoryByDate = new Button();
	 @FXML	 Button btnEditDeviceConfiguration = new Button();
	 @FXML	 Button btnSaveDeviceConfiguration = new Button();
	 @FXML	 DatePicker dtHistoryDate = new DatePicker();
	 @FXML	 DatePicker dtNextAppointment = new DatePicker();
	 @FXML	 RadioButton ManualMode = new RadioButton();
	 @FXML	 TableView<PatientHistory> tableView;
	 @FXML	 TableColumn<PatientHistory, String> phDate;
	 @FXML	 TableColumn<PatientHistory, String> phTime;
	 @FXML	 TableColumn<PatientHistory, Double> phGlucoseLevel ;
	 @FXML	 TableColumn<PatientHistory, Double> phDosage;
	 @FXML	 TableColumn<PatientHistory, String> phCriticality;	
	 @FXML CategoryAxis Time = new CategoryAxis();
	 @FXML	NumberAxis GlucoseLevel = new NumberAxis(); 
	 @FXML	LineChart<String,Number> HistoryChart = new LineChart<>(Time, GlucoseLevel);
	 @FXML Hyperlink hyperlink ;
	 @FXML private Button goBack;
	 private HostServices hostServices ;

	    
	 ViewsController controller;
	private final Main main = new Main() ;
	 @FXML  private void initialize()
	 {
		// hyperlink.setOnAction(e -> main.getHostServices().showDocument(hyperlink.getText()));
			cmbPatientID.setItems(DBCaller.selectPatientID("AllPatient"));
		 ModeBox.setValue("Manual");
		  ModeBox.getItems().addAll(
				 "Manual","AutoMatic");

	 }
	 public void goBack(ActionEvent event) throws IOException  {
			controller.setView(Util.screen1ID);
		}
	 
	public void LoadPatientProfile() 
	{
		   PatientProfile  patientProfile = DBCaller.selectPatientProfile("Current", cmbPatientID.getValue());
			txtpatientName.setText(patientProfile.getName());
			txtpatientAge.setText(Integer.toString(patientProfile.getAge()));
			txtpatientAddress.setText(patientProfile.getAddress());
			txtpatientAdditionalDrugs.setText(patientProfile.getAdditionalDrugs());			
			txtpatientContactNo.setText(patientProfile.getContactNo());
			System.out.println("patientProfile.isManual()......."+patientProfile.isManual());
			ModeBox.setValue((patientProfile.isManual()== true?"Manual":"Automatic"));

			//ManualMode.setSelected(true);
			txtpatientName.setEditable(false);
			txtpatientAge.setEditable(false);
			txtpatientAddress.setEditable(false);
			txtpatientAdditionalDrugs.setEditable(false);		
			txtpatientContactNo.setEditable(false);
			
	}
	
	public void btnAddNewPatient() {
		PatientProfile NewPatient = new PatientProfile();
		NewPatient.setName(txtpatientName.getText());
		NewPatient.setAge(Integer.parseInt(txtpatientAge.getText()));
		NewPatient.setAddress(txtpatientAddress.getText());
		NewPatient.setAdditionalDrugs(txtpatientAdditionalDrugs.getText());
		NewPatient.setContactNo(txtpatientContactNo.getText());
		NewPatient.setNextAppointment(dtNextAppointment.getValue().toString());
		System.out.println("ModeBox.getValue()...."+ModeBox.getValue());
		NewPatient.setManual((ModeBox.getValue() =="Manual"?true:false));
	if( DBCaller.InsertNewPatient(NewPatient)==1 )	
	  {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("New User");
			alert.setHeaderText("User Added Successfully!!");
			alert.showAndWait();
			cmbPatientID.setItems(DBCaller.selectPatientID("AllPatient"));	

		
	  }
	}
	
		
	public void OnPatientIDSelect()
	{
		LoadPatientProfile();
		patientHistory = DBCaller.selectPatientHistory("Today","",cmbPatientID.getValue());
		
		tableView.setItems(patientHistory);
		System.out.println(cmbPatientID.getValue());
		System.out.println(patientList.stream());
	}
	
	public void EnableAddtion() {
		txtpatientName.clear();
		txtpatientAge.clear();
		txtpatientAddress.clear();
		txtpatientAdditionalDrugs.clear();		
		txtpatientContactNo.clear();
		txtpatientName.setEditable(true);
		txtpatientAge.setEditable(true);
		txtpatientAddress.setEditable(true);
		ModeBox.setValue("Manual");
		txtpatientAdditionalDrugs.setEditable(true);		
		txtpatientContactNo.setEditable(true);
	}
	
	public void GetHistoryByDate() {
		System.out.println(dtHistoryDate.getValue().toString());/*patientHistory = DBCaller.selectPatientHistory("Date", dtHistoryDate.getValue().toString(), cmbPatientID.getValue());
		tableView.setItems(patientHistory);
		HistoryGraph(); */
	}
	
	public void SaveDeviceConfiguration() {
		System.out.println(dtHistoryDate.getValue().toString());
	/*	patientHistory = DBCaller.selectPatientHistory("Date", dtHistoryDate.getValue().toString(), cmbPatientID.getValue());
		tableView.setItems(patientHistory); */
	}
	
	
	public void HistoryGraph() {
		if (HistoryChart.getData().size() > 0) {
			HistoryChart.getData().remove(0);
		}
		if (patientHistory.size() > 0) {
			//Dialog dialog = new Dialog();
			//dialog.setTitle("Test");
			//dialog.setResizable(true);

			Time.setLabel("Time Of the Day");
			GlucoseLevel.setLabel("Glucose mg/dL");
			HistoryChart.setTitle("Glucose Variation Graph");
			HistoryChart.setCreateSymbols(false);
			Series<String, Number> series = new Series<>();
			series.setName("Glucose Variation");
	   
 for (PatientHistory History : patientHistory) {

	 series.getData().add(new Data<String, Number>(History.getTimeOfTheReading().getValue(), History.getBodyGlucose().getValue()));
}
	System.out.println(series.getData().size());
	if(series.getData().size()>0)
		HistoryChart.getData().add(series);
	
	//dialog.getDialogPane().setContent(HistoryChart);
	//ButtonType buttonTypeOk = new ButtonType("Okay", ButtonData.OK_DONE);
	//dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
	//dialog.showAndWait();      
	        
		}
		//}
	}

	@Override
	public void setViewParent(ViewsController viewPage) {
		controller =viewPage;

	}
	
	

    
    public void openURL() {
    	System.out.println(hyperlink.getText());
    	 Platform.runLater(() -> {
    		 hostServices.showDocument("www.google.com");
    		 //hostServices.showDocument(hyperlink.getText());
            });
			
       
    }
	}
	
	

