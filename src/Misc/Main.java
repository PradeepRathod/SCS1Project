package Misc;

import Controllers.ViewsController;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class Main extends Application {

    static AnchorPane root;
    static List<GridPane> grid = new ArrayList<GridPane>();

    @Override
    public void start(Stage primaryStage) {

        ViewsController mainContainer = new ViewsController();
           String id = "", name = "";
        mainContainer.loadView(Util.screen1ID, Util.screen1File);
        mainContainer.loadView(Util.screen2ID, Util.screen2File);
        mainContainer.loadView(Util.screen3ID, Util.screen3File);
        mainContainer.loadView(Util.screen4ID, Util.screen4File);
        mainContainer.loadView(Util.screen5ID, Util.screen5File);
        mainContainer.loadView(Util.screen6ID, Util.screen6File);
        mainContainer.loadView(Util.screen7ID, Util.screen7File);
        mainContainer.loadView(Util.screen8ID, Util.screen8File);




        mainContainer.setView(Util.screen1ID);

        Group root = new Group();
        root.getChildren().addAll(mainContainer);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);

    }


}
