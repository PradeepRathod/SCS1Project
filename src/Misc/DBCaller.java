package Misc;

import Model.PatientHistory;
import Model.PatientProfile;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;

//import java.text.SimpleDateFormat;
//import javafx.beans.property.SimpleStringProperty;
//import java.sql.Date;
//import application.Model.PatientDetails;


public class DBCaller {
	
	 public static ObservableList<PatientHistory> selectPatientHistory(String type, String Date, int PatientID){
		 String sql ="" ;
		 System.out.println(type);
		 switch(type)
		 {
		 case("All"):sql ="SELECT * FROM PatientHistory where PatientID ="+PatientID;break;
		 case("Today"): sql ="SELECT * FROM PatientHistory where DateOfReading= date('now') and PatientID ="+PatientID;break;
		 case("Date"): sql ="SELECT * FROM PatientHistory where DateOfReading= '"+Date+"' and PatientID ="+PatientID;break;
		 case("PatientID"): sql ="SELECT DISTINCT PatientID FROM PatientHistory";break;
		 }
		 	System.out.println(sql);
		 
	        ObservableList<PatientHistory> history = FXCollections.observableArrayList();
	        try{
	        	Connection conn = DBConnection.Connecter();
	             Statement stmt  = conn.createStatement();
	             ResultSet rs    = stmt.executeQuery(sql);
	             history =  PatientMapping.PatientHistory(rs);
	           }
	        catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	        return history;
	    }
	 
	 public static PatientProfile checkPatientMode(int PatientID)
	 {
			String sql = "SELECT isManual FROM PatientProfile where  PatientID =" + PatientID;
		 PatientProfile patientConfig= new PatientProfile();
			try
			{
				Connection conn = DBConnection.Connecter();
				Statement stmt  = conn.createStatement();
				ResultSet rs    = stmt.executeQuery(sql);
				patientConfig.setManual(rs.getBoolean("isManual"));
				}
			catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			return patientConfig;
		}
	 
	 public static PatientProfile selectPatientProfile(String type,int patientID){
		 String sql ="" ;
		 System.out.println(type);
		 switch(type)
		 {		
		 case("Current"): sql ="SELECT * FROM PatientProfile where  PatientID ="+patientID;break;
		 }			 
		 	PatientProfile patientProfile= new PatientProfile();
	        try
	        {
	        	 Connection conn = DBConnection.Connecter();
	             Statement stmt  = conn.createStatement();
	             ResultSet rs    = stmt.executeQuery(sql);
	             patientProfile.setName(rs.getString("Name"));
	             patientProfile.setAddress(rs.getString("Address"));
	             patientProfile.setAge(rs.getInt("Age"));
	             patientProfile.setAdditionalDrugs(rs.getString("AdditionalDrugs"));
	             patientProfile.setPatientID(patientID);
	             patientProfile.setContactNo(rs.getString("ContactNo"));
	        }
	        catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	        return patientProfile;
	    }
	 
	 public static ObservableList<Integer> selectPatientID(String type){
		 ObservableList<Integer> PatientIDs = FXCollections.observableArrayList();
		 String  sql = "";
		 switch(type)
		 {
		 case("AllPatient"):  sql ="SELECT DISTINCT PatientID FROM PatientProfile";break;
		 case("LastID"):  sql ="SELECT  Max(PatientID) AS PatientID FROM PatientProfile";break;
		 }
	        try
	        {
	        	 Connection conn = DBConnection.Connecter();
	             Statement stmt  = conn.createStatement();
	             ResultSet rs    = stmt.executeQuery(sql);
	             PatientIDs =  PatientMapping.PatientID(rs);
	        }
	        catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	        return PatientIDs;
	 }
	
	 public static int InsertNewPatient(PatientProfile patientProfile ){
		 
		 String sql = "INSERT INTO PatientProfile(name,Address,Age,AdditionalDrugs,ContactNo,nextAppointment,IsManual,Password) VALUES(?,?,?,?,?,?,?,?)";
		 int status = 2;
	        try (Connection conn = DBConnection.Connecter();
	            PreparedStatement pstmt = conn.prepareStatement(sql)) {
	            pstmt.setString(1, patientProfile.getName());
	            pstmt.setString(2, patientProfile.getAddress());
	            pstmt.setInt(3, patientProfile.getAge());
	            pstmt.setString(4, patientProfile.getAdditionalDrugs());
	            pstmt.setString(5, patientProfile.getContactNo());
				pstmt.setString(6, patientProfile.getNextAppointment());
				pstmt.setBoolean(7, patientProfile.isManual());
				pstmt.setString(8, "Patient123");
	          status =  pstmt.executeUpdate();
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	        System.out.println(status);
	        return status;
	       
	 }
	 
	 
	 /*public static int InsertNewDeviceConfiguration(DeviceConfiguration deviceConfiguration ){
		 
		 String sql = "INSERT INTO DeviceConfiguration(IsManual,NextAppointment,PatientID) VALUES(?,?,?)";
		 int status = 2;
	        try (Connection conn = DBConnection.Connecter();
	            PreparedStatement pstmt = conn.prepareStatement(sql)) {
	            pstmt.setBoolean(1, deviceConfiguration.isManual());
	            pstmt.setString(2, deviceConfiguration.getNextAppointment());
	            pstmt.setInt(3, deviceConfiguration.getPatientID());	           
	          status =  pstmt.executeUpdate();
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	        System.out.println(status);
	        return status;
	       
	 } */
	 
	 
 public static int InsertPatientHistory(PatientHistory patientHistory ){
		 
		 String sql = "INSERT INTO PatientHistory(GlucoseLevel,PatientID) VALUES(?,?)";
		 int status = 2;
		 
	        try (Connection conn = DBConnection.Connecter();
	            PreparedStatement pstmt = conn.prepareStatement(sql)) {
	        	pstmt.setDouble(1,Util.round(patientHistory.getBodyGlucose().get(),2));
	            pstmt.setInt(2, patientHistory.getPatientID().get());
	          status =  pstmt.executeUpdate();
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	        System.out.println(status);
	        return status;
	       
	 }
	 
	 

	 
	 
 public static PatientProfile GetPatientDetails(int PatientID ){
		 
		 String sql = "SELECT * FROM PatientProfile where  PatientID ="+PatientID;
	 PatientProfile patientConfig= new PatientProfile();
	        try
	        {
	        	 Connection conn = DBConnection.Connecter();
	             Statement stmt  = conn.createStatement();
	             ResultSet rs    = stmt.executeQuery(sql);
				patientConfig.setManual(rs.getBoolean("isManual"));
				patientConfig.setNextAppointment(rs.getString("NextAppointment"));
	         }
	        catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	        return patientConfig;
	      	 }
}
